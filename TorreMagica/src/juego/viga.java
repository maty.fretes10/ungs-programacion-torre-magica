package juego;

public class viga {
	private	double x;
	private double y;
	private double alto;
	private double ancho;

	public viga(int x, int y, int ancho, int alto){
		setX(x);
		setY(y);
		setAncho(ancho);
		setAlto(alto);
	}
	/**Guardar posiciones de las vigas para utilizarlas 
	en otras clases*/
	public double getX() {
		return x;
	}
	public double getY() {
		return y;
	}
	public double getAncho() {
		return ancho;
	}
	public double getAlto() {
		return alto;
	}
	
	public void setX(double x) {
		if (x>=0 && x<=600) {
			this.x = x;
		}
		else {
			throw new RuntimeException("Posicion de Viga no valida");
		}
	}
	public void setY(double y) {
		if (y>=0 && y<=800) {
			this.y = y;
		}
		else {
			throw new RuntimeException("Posicion de Viga no valida");
		}
	}
	public void setAncho(double ancho) {
		if (ancho>=0 && ancho<=600) {
			this.ancho = ancho;
		}
		else {
			throw new RuntimeException("Posicion de Viga no valida");
		}
	}
	public void setAlto(double alto) {
		if (alto>=0 && alto<=800) {
			this.alto = alto;
		}
		else {
			throw new RuntimeException("Posicion de Viga no valida");
		}
	}
}