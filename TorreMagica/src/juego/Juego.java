package juego;
import java.awt.Color;
import java.awt.Image;
import entorno.Entorno;
import entorno.Herramientas;
import entorno.InterfaceJuego;

public class Juego extends InterfaceJuego
{
	// El objeto Entorno que controla el tiempo y otros
	private Entorno entorno;
	
	// Variables y metodos propios de cada grupo
	// ...
	Disparo hechizos;
	Image imagenFondo;
	int vida = 3;
	int puntaje = 0;
	int cantMounstruos;
	int direccion;			// 0 = Derecha ; 1 = Izquierda;
	int cantDisparosFinal = 0;
	boolean fin = false;
	Mago mago;
	Monstruos[] monstruos;
	viga[] bloques;
	Tablero tablero;
	Bomba bomba;
	Monstruos monstruoFinal;
	
	
	Juego()
	{
		// Inicializa el objeto entorno
		this.entorno = new Entorno(this, "Torre Magica - Grupo 8 - Fretes Matias - Ortiz Agustin - V0.01", 600, 800);
		
		//Inicializando personajes y vigas
		mago = new Mago(300, 35,40,40);
		monstruos = new Monstruos[4];
		bloques = new viga[9];
		this.cantMounstruos=monstruos.length;
		tablero = new Tablero();
		monstruoFinal = new Monstruos(300,450,20,20);
		
		
		// Inicializar lo que haga falta para el juego
		// ...
			// Inicializando imagenes
		imagenFondo = Herramientas.cargarImagen("volcan.png");
		


			//Inicializando vigas
		bloques[0] = new viga(300,125,350,30);
		bloques[1] = new viga(115,250,230,30);
		bloques[2] = new viga(485,250,230,30);
		bloques[3] = new viga(300,400,250,30);
		bloques[4] = new viga(300,550,350,30);
		bloques[5] = new viga(525,700,175,30);
		bloques[6] = new viga(75,700,175,30);
		bloques[7] = new viga(75,10,175,30);
		bloques[8] = new viga(525,10,175,30);
		
			//Inicializando monstruos
		monstruos[0] = new Monstruos(20,217,40,40);
		monstruos[1] = new Monstruos (580,217,40,40);
		monstruos[2] = new Monstruos(20,667,40,40);
		monstruos[3] = new Monstruos(580,667,40,40);

		
		// Inicia el juego!
		this.entorno.iniciar();
	}

	/**
	 * Durante el juego, el metodo tick() sera ejecutado en cada instante y 
	 * por lo tanto es el metodo mas importante de esta clase. Aqui se debe 
	 * actualizar el estado interno del juego para simular el paso del tiempo 
	 * (ver el enunciado del TP para mayor detalle).
	 */
	
	public void tick()
	{
			if(vida>0) {	
				//Colocar imagen del fondo
				entorno.dibujarImagen(imagenFondo, 300, 350,0);

				//Dibujar vigas
				for (int i = 0; i < bloques.length; i++) {
					entorno.dibujarRectangulo(bloques[i].getX(),bloques[i].getY(),bloques[i].getAncho(),bloques[i].getAlto(), 0, Color.ORANGE);
				}
				//Dibujar Mago, sus acciones
				mago.dibujarse(entorno);			
				if(mago.seSolapanMagoViga(bloques)) {
					mago.movimientoMago(entorno, mago);						//El mago solo camina cuando esta encima de la viga	
					if(entorno.sePresiono(entorno.TECLA_CTRL)) {
						bomba = new Bomba(mago.getX(), mago.getY()+15,20);
					}
					if(entorno.sePresiono(entorno.TECLA_ESPACIO)) {
						mago.salto(entorno);
					}
				}			
				else{														// Inicializacion del hechizo, y se desplaza cuando mago toca viga 
					mago.moverAbajo();
					hechizos = new Disparo(mago.getX(),mago.getY(),10,10);	//Genera el disparo	
					if(entorno.sePresiono(entorno.TECLA_DERECHA)) {			//Activa disparo hacia la derecha(Lo activa mientras cae)
						this.direccion = 0;
					}
					if(entorno.sePresiono(entorno.TECLA_IZQUIERDA)) {		//Activa disparo hacia la izquierda(Lo activa mientras cae)
						this.direccion = 1;
					}
				}
				
					for (int i = 0; i < monstruos.length; i++) {	
						//Ejecuta disparo y bomba 
						if(hechizos != null) {								//Si el hechizo esta inicializado, se dibuja y se desplaza
							hechizos.dibujarse(entorno);
							hechizos.desplazamiento(hechizos,direccion);
						}		
						if(bomba != null) {
							bomba.AccionesBomba(bomba, mago, entorno, bloques, direccion);
						}
						
						
						if(cantMounstruos>0) {
							
							switch(monstruos[i].getEstado()) {					//Dependiendo del estado del monstruo, se generan diversas acciones.
						
						  	case 0: // Monstruos vivos 
						  		monstruos[i].EstadoVivo (monstruos[i], bloques, entorno);
						  		if(mago.seSolapanMagoMonstruo(monstruos[i])) {			
						  			monstruos[i].EstadoInicialPersonajes(monstruos, mago);
						  			this.vida -=1;
						  			break;
						  		}
						  		

						  		if(hechizos.seSolapanHechizo(monstruos[i])){			//Si colisiona el hechizo y el monstruo, este ultimo
									monstruos[i].setEstado(1);							//Pasa al caso 1 (Monstruo Congelado) y se le suma el puntaje
									monstruos[i].setTiempoStop();
									hechizos.y = 801;									//Hechizo sale de la pantalla al tocar con un monstruo.
									puntaje = puntaje + 260;
									
								}
						  		if(bomba != null) {
							  		if(bomba.seSolapanBombaMoustros(monstruos[i])) {
							  			monstruos[i].setEstado(1);
							  			monstruos[i].setTiempoStop();
							  			bomba.y = 805;
							  			puntaje = puntaje + 500;
							  			
							  		}
						  		}
									
							    break;
						    
						  case 1:	// Monstruos congelados
								monstruos[i].EstadoCongelado(entorno, mago, monstruos[i]);
							    break;
						    
						  case 2:	// Monstruos rodando 
							  	monstruos[i].EstadoRodando(monstruos[i], entorno, bloques);

							  	if(monstruos[i].y>700) {								//El monstruo llega al final de la pantalla
							  		monstruos[i].setEstado(3);							//y pasa al caso 3 (Default) es decir, se elimina al monstruo
							  		this.cantMounstruos-=1;								//la cantidad de monstruo disminuye
							  		this.puntaje += 1000;								//Aumenta el puntaje
							  	}
							  	
							  	for (int j = 0; j < 4; j++) {							
							  		if(monstruos[i].MonstruoAMonstruo(monstruos[j])) {		//Recorre los monstruos del estado 2, y los compara con el resto
							  			if(monstruos[j].getEstado() == 0) {					//Si se solapa con un monstruo vivo, lo congela y lo hace rodar hasta
							  				monstruos[j].setEstado(2);						//Eliminarlo. Incrementa puntuacion.
							  				this.puntaje += 1500;
							  			}
							  		}
								}
							  	
							  	break;

						  default:														//El monstruo desaparece de la pantalla.
							  	break;
						}
					}
				}	
					
					
				// LA FINAL
					
					if((this.cantMounstruos == 0)&&(this.vida>0)) {	
						
						if(hechizos.seSolapanHechizoMonstruoFinal(monstruoFinal)) {
			  				this.cantDisparosFinal += 1;
			  				hechizos.y = 801;
			  				puntaje += 3000;
			  				monstruoFinal.setEstado(cantDisparosFinal);
			  			}
						
						
							
						
						switch(monstruoFinal.getEstado()) {	
					  		case 0: 			
					  			monstruoFinal.MonstruoFinalEstado3Cabezas(hechizos,monstruoFinal, bloques, entorno);
					  			if(mago.seSolapanMagoMonstruoFinal(monstruoFinal)) {
									monstruoFinal.PosInicialFinal(mago);
									this.vida -= 1;
								}
					  			break;
			
					  		case 1:
					  			monstruoFinal.MonstruoFinalEstado2Cabezas(hechizos, monstruoFinal, entorno, bloques);
					  			if(mago.seSolapanMagoMonstruoFinal(monstruoFinal)) {
									monstruoFinal.PosInicialFinal(mago);
									this.vida -= 1;
								}
					  			break;
					  			
					  		case 2:
					  			monstruoFinal.MonstruoFinalEstado1Cabeza(hechizos,monstruoFinal,entorno, bloques);
					  			if(mago.seSolapanMagoMonstruoFinal(monstruoFinal)) {
									monstruoFinal.PosInicialFinal(mago);
									this.vida -= 1;
								}
					  			
					  			break;

					  		case 3:																		
							  	this.fin = true;
							  	break;
							  	


					  		default:
					  			break;
					  			
						}
					}
				//INICIANDO TABLERO	
					tablero.dibujarse(entorno, vida, puntaje);	
				
				
				//VICTORIA
					if((this.vida>0)&& fin == true) {
						tablero.VICTORIA(entorno, puntaje);
					}
				
				}

			//GAMEOVER, Dibuja imagen y el puntaje final
			else {
				tablero.DERROTA(entorno, puntaje);
			}
	}
		// Procesamiento de un instante de tiempo
		// ...
	@SuppressWarnings("unused")
	public static void main(String[] args)
	{	
		Juego juego = new Juego();
	}
}

