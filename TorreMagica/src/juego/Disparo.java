package juego;

import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Disparo {
	double x;
	double y;
	double ancho;
	double alto;
	Image ImagenHechizo;
	
	public Disparo(double x, double y,double ancho, double alto) {
		this.x = x;
		this.y = y;
		this.ancho=ancho;
		this.alto=alto;
		ImagenHechizo = Herramientas.cargarImagen("hechizo.png");

	}
	public void dibujarse(Entorno entorno) {
		entorno.dibujarImagen(ImagenHechizo, x, y, 0, 1);	
	}
	
	public void desplazamiento(Disparo disparo, int a) {
		if(a==0) {
			x+=5;
		}
		else {
			x-=5;
		}
		
	}
	
	public boolean seSolapanHechizo(Monstruos monstruo) {
			if((x>monstruo.posX()-monstruo.posAncho()/2-ancho/2) 
					&& (x < monstruo.posX()+ monstruo.posAncho()/2 + ancho/2) 
					&& (y+20>monstruo.posY() - monstruo.posAlto()/2) 
					&& (y+20<monstruo.posY() + monstruo.posAlto())) {
				return true;
			}
		return false;
	}
	
	public boolean seSolapanHechizoMonstruoFinal(Monstruos monstruoFinal) {
		if((x>monstruoFinal.posX()-monstruoFinal.posAncho()/2-ancho/0.5) 
				&& (x < monstruoFinal.posX()+ monstruoFinal.posAncho()/2 + ancho/0.5) 
				&& (y+20>monstruoFinal.posY() - monstruoFinal.posAlto()*2) 
				&& (y+20<monstruoFinal.posY() + monstruoFinal.posAlto()*2)) {
			return true;
		}
	return false;
	}


	
}