package juego;


import java.awt.Image;
import java.util.Date;

import entorno.Entorno;
import entorno.Herramientas;

public class Monstruos {
	    double x;
		double y;
	    double ancho;
	    double alto;
		int vel =1;
		int velCongelado = 3;
		int cantDisparos = 0;
		Image imagenMonstruo;
		Image imagenMonstruoCongelado;
		Image fin3vidas;
		Image fin2vidas;
		Image fin1vida;
		private int estado; 
		Bomba bomba;
		long tiempoStop;

		public Monstruos(int x, int y, int ancho, int alto){
			this.x = x;
			this.y= y;
			this.ancho=ancho;
			this.alto=alto;
			this.estado = 0;
			imagenMonstruo = Herramientas.cargarImagen("monstruo.png");
			imagenMonstruoCongelado = Herramientas.cargarImagen("monstruohelado.png");
			fin3vidas = Herramientas.cargarImagen("3VidaMonstruo.png");
			fin2vidas = Herramientas.cargarImagen("2VidaMonstruo.png");
			fin1vida = Herramientas.cargarImagen("1VidaMonstruo.png");
			this.tiempoStop=0;
		}
		//Si el monstruo esta vivo
		public void dibujarse(Entorno entorno){
			entorno.dibujarImagen(imagenMonstruo, x, y, 0, 1);	
		}
		//Si el monstruo esta congelado
		public void dibujarCongelado(Entorno entorno) {
			entorno.dibujarImagen(imagenMonstruoCongelado, x, y, 0, 1);	
		}
		//Si el monstruo esta congelado
		public void dibujarCongeladoRodando(Entorno entorno) {
			entorno.dibujarImagen(imagenMonstruoCongelado, x, y, vel++);	    
       }		

		/**Guardar posiciones de las monstruos para utilizarlas 
		en otras clases*/
		public double posX() {
			return x;
		}
		public double posY() {
			return y;
		}
		public double posAncho() {
			return ancho;
		}
		public double posAlto() {
			return alto;
		}
		public int getEstado() {
			return estado;
		}
		public void setEstado(int estado) {
			this.estado = estado;
		}
		public long getTiempoStop() {
        	return this.tiempoStop;
        }
		
		public void setTiempoStop() {
        	Date dato1=new Date();
        	this.tiempoStop = dato1.getTime()/1000L;		
        }
		
		public long TiempoStopTranscurrido() {
        	long  resta,r1; 
        	Date actual=new Date();
        	r1 = actual.getTime()/1000L;
        	resta =r1-this.tiempoStop;
        	return resta;
        }
		
		//ACCIONES MONSTRUOS
		public boolean MonstruoAMonstruo(Monstruos montruos) {
				if((x>montruos.posX()-montruos.posAncho()/2-ancho/2) 
					&& (x < montruos.posX()+ montruos.posAncho()/2 + ancho/2 ) 
					&& (y+20>montruos.posY() - montruos.posAlto()/2) 
					&& (y+20<montruos.posY() + montruos.posAlto())) {
					return true;
				}
			
			return false;
		}
		
		public boolean seSolapanMonstruoViga(viga[] viga) {
			for (int i = 0; i < viga.length; i++) {
				if((x>viga[i].getX()-viga[i].getAncho()/2-ancho/2) 
						&& (x < viga[i].getX()+ viga[i].getAncho()/2 + ancho/2 ) 
						&& (y+20>viga[i].getY() - viga[i].getAlto()/2) 
						&& (y+20<viga[i].getY() + viga[i].getAlto())) {
					return true;
				}
			}
			return false;
		}
		public void movimiento() {
				if(x+vel < 20){
					vel =-1;
				}
				else if(x+vel>580){
					vel= 1;
				}
				x-=vel;	
		}
		public void moverAbajo() {
			if((this.y>0)&&(this.y<750)) {
				this.y +=2;
			}
			else {
				this.y =15;
			}
		}
		public void moverAbajoCongelado() {
			if((this.y>0)&&(this.y<800)) {
				this.y +=5;
			}
		}
		
		public void movimientoCongelado() {
			if(x+velCongelado < 20){
				velCongelado = -5;
			}
			else if(x+velCongelado>580){
				velCongelado= 5;
			}
			x-=velCongelado;	
		}
		
		public void EstadoInicialPersonajes(Monstruos[] monstruos, Mago mago) {
			for (int k = 0; k < 4; k++) {
					mago.posInicial();
					monstruos[0].y = 217;								//Posiciones iniciales de los monstruos
					monstruos[0].x = 20;
					monstruos[1].y = 217;
					monstruos[1].x = 580;
					monstruos[2].y = 667;
					monstruos[2].x = 20;
					monstruos[3].y = 667;
					monstruos[3].x = 580;
				}
			
		}
		
		public void EstadoVivo(Monstruos monstruos, viga[] bloques, Entorno entorno) {
				monstruos.dibujarse(entorno);						//Se dibujan los monstruos vivos y se desplazan
			  	if(!monstruos.seSolapanMonstruoViga(bloques) ) {
				monstruos.moverAbajo();
				}
				else {
					monstruos.movimiento();
				} 				
		}
	
		public void EstadoCongelado(Entorno entorno,Mago mago, Monstruos monstruos) {
			monstruos.dibujarCongelado(entorno);  				//Se dibuja el monstruo congelado		
			
			if(mago.seSolapanMagoMonstruo(monstruos)) {			//Si el mago toca al monstruo congelado, este ultimo pasa al
				monstruos.setEstado(2);							//caso 2 (Monstruo rodando)	
			}
			if (TiempoStopTranscurrido()>2) {             //si el tiempo es mayor a 2 seg se descongela
				monstruos.setEstado(0);
				this.tiempoStop=0;
			}
		
		}
		
		public void EstadoRodando(Monstruos monstruos, Entorno entorno, viga[] bloques) {
			if(!monstruos.seSolapanMonstruoViga(bloques) ) {		//El monstruo congelado toma movilidad, y gira sin parar hasta 
				monstruos.moverAbajoCongelado();					//llegar al final de la pantalla
			}
			else {
				monstruos.movimientoCongelado();
			} 	
		  	monstruos.dibujarCongeladoRodando(entorno); 			//Se dibuja el monstruo congelado y aumenta el angulo indefinidamente
		}
		
		
		//////// MONSTRUO FINAL
		
		public void PosInicialFinal(Mago mago) {
			mago.posInicial();
			x = 300;
			y = 450;
		}
		
		public void dibujarseSegunVida3(Entorno entorno){
			entorno.dibujarImagen(fin3vidas, x, y, 0, 0.8);	
		}	
		public void dibujarseSegunVida2(Entorno entorno){
			entorno.dibujarImagen(fin2vidas, x, y, 0, 0.8);	
		}
		public void dibujarseSegunVida1(Entorno entorno){
			entorno.dibujarImagen(fin1vida, x, y, 0, 0.8);	
		}

		public boolean seSolapanMonstruoFinalConViga(viga[] viga) {
			for (int i = 0; i < viga.length; i++) {
				if((x>viga[i].getX()-viga[i].getAncho()/2-ancho/0.5) 
						&& (x < viga[i].getX()+ viga[i].getAncho()/2 + ancho/0.5 ) 
						&& (y+20>viga[i].getY() - viga[i].getAlto()*0.7) 
						&& (y+20<viga[i].getY() + viga[i].getAlto())) {
					return true;
				}
			}
			return false;
		}
		
		public void movimientoDelEstado2() {
			if(x+vel < 20){
				vel =-2;
			}
			else if(x+vel>580){
				vel= 2;
			}
			x-=vel;	
		}

		public void movimientoDelEstado3() {
			if(x+vel < 20){
				vel =-5;
			}
			else if(x+vel>580){
				vel= 5;
			}
			x-=vel;	
		}


		public void MonstruoFinalEstado3Cabezas(Disparo hechizos, Monstruos monstruoFinal, viga[] bloques, Entorno entorno) {
			monstruoFinal.dibujarseSegunVida3(entorno);
		  	if(!monstruoFinal.seSolapanMonstruoFinalConViga(bloques) ) {
		  		monstruoFinal.moverAbajo();
			}
			else {
				monstruoFinal.movimiento();
			}

		} 
		
		public void MonstruoFinalEstado2Cabezas(Disparo hechizos,Monstruos monstruoFinal2, Entorno entorno, viga[] bloques) {
			if(!monstruoFinal2.seSolapanMonstruoFinalConViga(bloques) ) {		
				monstruoFinal2.moverAbajo();										
			}
			else {
				monstruoFinal2.movimientoDelEstado2();
			} 	
			monstruoFinal2.dibujarseSegunVida2(entorno); 
		}
		
		public void MonstruoFinalEstado1Cabeza(Disparo hechizos,Monstruos monstruoFinal3, Entorno entorno, viga[] bloques) {
			if(!monstruoFinal3.seSolapanMonstruoFinalConViga(bloques) ) {		
				monstruoFinal3.moverAbajo();										
			}
			else {
				monstruoFinal3.movimientoDelEstado3();
			} 	
			monstruoFinal3.dibujarseSegunVida1(entorno); 
		
		}
	
}