package juego;

//import java.awt.Color;
import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Bomba {
	double x;
	double y;
	double diametro;
	Image bomba;

	
	public Bomba(double x, double y, double diametro) {
		this.x = x;
		this.y = y;
		this.diametro = diametro;
		bomba = Herramientas.cargarImagen("bomba.png");
	}
	
	public void dibujarse(Entorno entorno) {	
		entorno.dibujarImagen(bomba, x, y, 0, 1);	
	}
	
	public void AccionesBomba(Bomba bomba, Mago mago, Entorno entorno, viga [] bloques, int direccion) {
		bomba.dibujarse(entorno);
		if(direccion == 0) {
			if(x>0 && x<=600) {
				if(bomba.x == 600) {
					bomba.y = 805;
				}
				if (bomba.seSolapanBombaViga(bloques)) {
					x += 1;
				}
				else {
					bomba.CaerBomba();
				}
			}
		}
		if(direccion == 1) {
			if(x>=0 && x<=600) {
				if(bomba.x == 0) {
					bomba.y = 805;
				}
				if (bomba.seSolapanBombaViga(bloques)) {
					x -= 1;
				}
				else {
					bomba.CaerBomba();
				}
			}
		}
	}
	
	public void CaerBomba() {
		if((this.y>0)&&(this.y<750)) {
			this.y +=1;
		}
	}	
	
	public boolean seSolapanBombaMoustros(Monstruos monstruo) {
		if((x>monstruo.posX()-monstruo.posAncho()/2-diametro) 
				&& (x < monstruo.posX()+ monstruo.posAncho()/2 + diametro) 
				&& (y+20>monstruo.posY() - monstruo.posAlto()/2) 
				&& (y+20<monstruo.posY() + monstruo.posAlto())) {
			return true;
		}
	return false;
	}
	

	public boolean seSolapanBombaViga(viga[] viga) {
		for (int i = 0; i < viga.length; i++) {
			if((x>viga[i].getX()-viga[i].getAncho()/2-diametro) 
					&& (x < viga[i].getX()+ viga[i].getAncho()/2 + diametro ) 
					&& (y+20>viga[i].getY() - viga[i].getAlto()/2) 
					&& (y+20<viga[i].getY() + viga[i].getAlto())) {
				return true;
			}
		}
		return false;
	}
}
