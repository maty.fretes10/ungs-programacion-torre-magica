package juego;

import java.awt.Image;
import entorno.Entorno;
import entorno.Herramientas;

public class Mago {
	private double x;
	private double y;
	private double alto;
	private double ancho;
	Image imagenMago;
	

	public Mago(int x, int y, int ancho, int alto) {
		setX(x);
		setY(y);
		setAncho(ancho);
		setAlto(alto);
		imagenMago = Herramientas.cargarImagen("mago.png");
	}
	/**Guardar posiciones del mago para utilizarlas 
	en otras clases*/
	public double getX() {
		return x;
	}
	public double getY() {
		return y;
	}
	public double getAncho() {
		return ancho;
	}
	public double getAlto() {
		return alto;
	}
	public void setX(double x) {
		this.x= x;
	}
	public void setY(double y) {
		this.y=y;
	}
	public void setAncho(double ancho) {
		this.ancho = ancho;
	}
	public void setAlto(double alto) {
		this.alto = alto;
	}
	
	public void posInicial() {
		this.x =300;
		this.y=15;
		this.ancho= 40;
		this.alto = 40;
	}
	
	public void dibujarse(Entorno entorno){
		entorno.dibujarImagen(imagenMago, x, y, 0, 1);	
		}

	//ACCIONES MAGO
	public boolean seSolapanMagoViga(viga[] viga) {
		for (int i = 0; i < viga.length; i++) {
			if((x>viga[i].getX()-viga[i].getAncho()/2-ancho/2) 
					&& (x < viga[i].getX()+ viga[i].getAncho()/2 + ancho/2 ) 
					&& (y+20>viga[i].getY() - viga[i].getAlto()/2) 
					&& (y+20<viga[i].getY() + viga[i].getAlto())) {
				return true;
			}
		}
		return false;
	}
	
	public boolean seSolapanMagoMonstruo(Monstruos monstruo) {
		
		if((x>monstruo.posX()-monstruo.posAncho()/2-ancho/2) 
				&& (x < monstruo.posX()+ monstruo.posAncho()/2 + ancho/2 ) 
				&& (y+20>monstruo.posY() - monstruo.posAlto()/2) 
				&& (y+20<monstruo.posY() + monstruo.posAlto())) {
			return true;
		}
		
		return false;
	}
	
	public boolean seSolapanMagoMonstruoFinal(Monstruos monstruoFinal) {
		
		if((x>monstruoFinal.posX()-monstruoFinal.posAncho()/2-ancho/2) 
				&& (x < monstruoFinal.posX()+ monstruoFinal.posAncho()/2 + ancho/2) 
				&& (y+20>monstruoFinal.posY() - monstruoFinal.posAlto()*2) 
				&& (y+20<monstruoFinal.posY() + monstruoFinal.posAlto()*2)) {
			return true;
		}
		
		return false;
	}

	public void moverAbajo() {
		if((this.y>0)&&(this.y<750)) {
			this.y +=2;
		}
		else {
			this.y =15;
		}
	}
	
	public void movimientoMago(Entorno entorno, Mago mago) {
		if (entorno.estaPresionada(entorno.TECLA_DERECHA)) {
			mago.moverDerecha();
		}
		if (entorno.estaPresionada(entorno.TECLA_IZQUIERDA)) {
			mago.moverIzquierda();
		}
	}
	public void moverDerecha(){	
		if ((this.x>0)&&(this.x<585)){
			this.x += 2;
		}
		else{
			this.x -=50;
		}
	}
	public void moverIzquierda(){
		if ((this.x>15)&&(this.x<600)){
			this.x -= 2;
		}
		else{
			this.x +=50;
		}
	}
	
	public void salto(Entorno entorno) {
		if (!entorno.estaPresionada(entorno.TECLA_DERECHA) && !entorno.estaPresionada(entorno.TECLA_IZQUIERDA)) {
			for (int i = 0; i < 10; i++) {
				y-=i;
			}
		}
		
		if (entorno.estaPresionada(entorno.TECLA_DERECHA)) {
			for (int i = 0; i < 10; i++) {
				y-=i;
				x += i;
			}
		}
		if (entorno.estaPresionada(entorno.TECLA_IZQUIERDA)) {
			for (int i = 0; i < 10; i++) {
				y-=i;
				x -= i;
			}
		}
		
	}
	
	
}