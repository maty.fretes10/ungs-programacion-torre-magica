package juego;

import java.awt.Color;
import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Tablero {
	Image tablero;
	Image TresVidas;
	Image DosVidas;
	Image UnaVida;
	Image victoria;
	Image gameover;
	
	
	public Tablero(){	
		TresVidas =  Herramientas.cargarImagen("TresVidas.png");
		DosVidas =  Herramientas.cargarImagen("DosVidas.png");
		UnaVida =  Herramientas.cargarImagen("UnaVida.png");
		tablero = Herramientas.cargarImagen("lava.png");
		victoria = Herramientas.cargarImagen("Ganador.png");
		gameover =  Herramientas.cargarImagen("gameover.jpg");

	}
	
	public void dibujarse(Entorno entorno, int vida, int puntaje){
		//Inicializando tablero
		entorno.dibujarImagen(tablero, 295, 728, 0);
		
		entorno.dibujarRectangulo(100, 755, 200, 50, 0, Color.BLACK);
		//Inicializando imagenes de vidas
		if(vida == 3) {
			entorno.dibujarImagen(TresVidas, 110, 755, 0);
		}
		else if(vida == 2) {
			entorno.dibujarImagen(DosVidas, 110, 755, 0);
		}
		else if(vida == 1) {
			entorno.dibujarImagen(UnaVida, 110, 755, 0);
		}
		//Puntaje mostrado por pantalla
		entorno.cambiarFont("Arial Black", 35, Color.WHITE);
		entorno.escribirTexto("PUNTAJE: "+puntaje, 225, 765);	
	}
	
	public void VICTORIA (Entorno entorno, int puntaje){
		entorno.dibujarImagen(victoria, 300, 400,0);						//Carga la pantalla de victoria
		entorno.cambiarFont("Arial Black", 50, Color.WHITE);				//Muestra el puntaje por pantalla
		entorno.escribirTexto(""+puntaje, 240, 575);
	}
	
	public void DERROTA (Entorno entorno, int puntaje){
		entorno.cambiarFont("Arial Black", 25, Color.WHITE);
		entorno.dibujarImagen(gameover, 300, 350,0);
		entorno.escribirTexto("PUNTAJE FINAL: "+puntaje, 150, 650);
	}
	
}
